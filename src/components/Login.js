import React, { Component } from 'react';
import './../App.css';
import {
  NavLink,
} from 'react-router-dom'

class Login extends Component {
  constructor(props) {
    super(props);
    this.enteringEmail = this.enteringEmail.bind(this);
    this.enteringPassword = this.enteringPassword.bind(this);
  }

  enteringEmail() {
    console.log("email");
  }

  enteringPassword() {
    console.log("password");
  }

  render() {
    return (
      <div className="login">
        <div>
          <label htmlFor="e-mail">E-mail</label>
          <input type="email" onInput={this.enteringEmail}/>
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input type="password" onInput={this.enteringPassword}/>
        </div>
        <NavLink to={"/Wall"}><button disabled>LOGIN</button></NavLink>
      </div>
    )
  }
}
  

export default Login;