import React from 'react';
import Home from './components/Home';
import Header from './components/Header';
import Wall from './components/Wall';
import Footer from './components/Footer';
import './App.css';
import {
	BrowserRouter,
	Route,
  Switch
} from 'react-router-dom';

const App = (props) => {
	return (
    <BrowserRouter>
      <div className="App">
        <Header/>
        <Switch>
          <Route  path="/Home" render={() => <Home/>}/>
          <Route  path="/Wall" render={() => <Wall/>}/>
          <Route component={Home}/>
        </Switch>
        <Footer/>
      </div>
    </BrowserRouter>)
}


export default App;
